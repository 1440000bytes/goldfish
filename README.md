# Goldfish

![screenshot](https://i.imgur.com/aQj3jU5.png)

## Description

A tool for spoofing wallet fingerprints of bitcoin transactions (unsigned PSBT).

## Usage

```
$ npm install bitcoinjs-lib browserify
$ npx browserify --standalone bitcoin - -o bitcoinjs-lib.js <<<"module.exports = require('bitcoinjs-lib');"
$ python -m http.server 8080
```

Clone repository and open `index.html` in browser.

## Features

This is a proof of concept which only supports nLocktime, nVersion and BIP 69 ordering for 2 bitcoin wallets (electrum and blue).

## Contributing

Feel free to add more fingerprints by creating pull requests.


